const path = require("path");

module.exports = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      }
    ],
    loaders: [
      {
        test: /\.json$/,
        loader: "json-loader"
      }
    ],
    resolve: {
      alias: {
        "@": path.resolve("src")
      }
    }
  }
};
