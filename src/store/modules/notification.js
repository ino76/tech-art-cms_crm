let nextID = 0;

export default {
  state: {
    notifications: []
  },
  mutations: {
    PUSH(state, notification) {
      state.notification.push({
        ...notification,
        id: nextID++
      });
    }
  }
};
