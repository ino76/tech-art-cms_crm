export default {
    namespaced: true,
    state: {
        isLogged: false,
        user: {
            firstName: "",
            lastName: "",
            email: "",
            phone: "",
            locality: "",
            category: "",
            aviable_date: "",
            linkedIn: ""
        }
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
        setLogin(state, value) {
            // console.log(`mutation setLogin: ${value}`)
            state.isLogged = value
        }
    },
    actions: {
        loginUser({ commit }, bool) {
            // console.log(`action loginUser: ${bool}`)
            commit("setLogin", bool)
        },
        setUser({ commit }, user) {
            commit("SET_USER", user);
        }
    },
    getters: {
        isLogged(state) {
            return state.isLogged;
        }
    }
};
