import Vue from "vue";
import Vuex from "vuex";

import state from "./state";
import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

import user from "./modules/user";

Vue.use(Vuex);

export default new Vuex.Store({
    // strict mode throws an error when state is chnges 'wrong way' .. it should be turned off on production
    // strict: process.env.NODE_ENV !== 'production',
    modules: {
        user
    },
    state,
    getters,
    mutations,
    actions
});

// new module template

// export default {
//     namespaced: true,
//     state: {},
//     mutations: {},
//     actions: {},
//     getters: {}
//   };
