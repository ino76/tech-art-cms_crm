import axios from "axios";

// TODO: REPLACE WITH ACUAL HEADERS AND DOMAIN ... THIS IS JUST AN EXAMPLE ;)
const apiClient = axios.create({
  baseURL: "http://localhost:8080/",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-type": "application/json"
  }
});

// TODO: REPLACE WITH ACUAL METHODS ... THIS IS JUST AN EXAMPLE ;)
export default {
  callSomeApiMethod() {
    return apiClient.get("/users/");
  },
  getUser(id) {
    return apiClient.get("/users/" + id);
  }
};

// usage in actual file:
// import EventService from '@/services/EventService'
// ...
// ...some code
// ...
// created() {
//     EventService.getUser(2)
//         .then(response => {
//             this.event = response.data
//         })
//         .catch(err => {
//             console.log("there was some error: " + err)
//         })
// }
