import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import NProgress from "nprogress";
import store from "./store/store.js";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
            meta: { requiresLogin: false }
        },
        {
            path: "/login",
            name: "login",
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import(/* webpackChunkName: "login" */ "./views/Login.vue"),
            meta: { requiresLogin: false }
        },
        {
            path: "/prehled",
            name: "prehled",
            component: () => import("./views/Prehled.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/doklady",
            name: "doklady",
            component: () => import("./views/Doklady.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/firmy",
            name: "firmy",
            component: () => import("./views/Firmy.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/kontakty",
            name: "kontakty",
            component: () => import("./views/Kontakty.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/zakazky",
            name: "zakazky",
            component: () => import("./views/Zakazky.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/ucet",
            name: "ucet",
            component: () => import("./views/Ucet.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/nastaveni",
            name: "nastaveni",
            component: () => import("./views/Nastaveni.vue"),
            meta: { requiresLogin: true }
        }
    ]
});

router.beforeEach((routeTo, routeFrom, next) => {
    NProgress.start();
    // console.log(`user is logged in: ${store.state.user.isLogged}`)

    // console.log(`require login: ${routeTo.meta.requiresLogin}`)

    if (store.state.user.isLogged && routeTo.meta.requiresLogin) {
        next();
    } else if (!routeTo.meta.requiresLogin) {
        next();
    } else {
        router.push('/login')
    }
});

router.afterEach(() => {
    NProgress.done();
});

export default router;
