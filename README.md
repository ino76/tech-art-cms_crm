# tech-art-cms_crm 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Git add -A then COMMIT and then PUSH (with auto-message)
```
npm run push
```

### First build and then call surge for deploy (you must have installed surge: 'npm i -g surge')
```
npm run surge
```

### Add all chages to git and commit with auto-message, then build and call surge for deploy (you must have installed surge: 'npm i -g surge')
```
npm run all
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
